package Constructors

class MyClass {
  println("Hey I'm some random code not sitting in a method... or am I?")

  def method() = {
  }
}

class PrimaryCtorWithProperties(val name: String, val age : Int) {
  def method() = {
    println(s"$name and $age are actually public properties")
  }
}

class PrivateProperties(name: String, age : Int) {
  def method() = {
    println(s"$name and $age are private properties")
    //And of course, in FP, they are immutable!
    // name = "bar" .. fail
  }
}

class PublicMutableProperties(var name: String, var age : Int) {
    def method() = {
      println(s"$name and $age are public mutable properties")
      name = "Going straight to FP Hell ..."
    }
}

class AuxilliaryConstructor(var name: String, var age : Int) {
  def this() = this("Defaulted", 0)
}