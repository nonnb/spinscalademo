package PatternMatching

// Unlike 'class', case class CTor properties are public val by default
case class PersonCaseClass(name: String, age : Int) {
  // Get "Free" unapply for pattern matching
  // Get free hashCode and equals using all properties
}

class MatchCaseClass {
  // Some new random FaceBook BS app "How Cool are you?"
  def rankPerson(person : PersonCaseClass): Int = {
    person match {
      case PersonCaseClass(name, _)   if name.startsWith("Superman") => 100
      case PersonCaseClass(name, _)   if name.endsWith("Esquire") => 80
      case PersonCaseClass(_, age)    if age < 50 => 50
      case PersonCaseClass(name, age) if age > 30 && !name.contains("Idiot") => 20
      case _ => 0
    }
  }

  def notAGoodIdea(person : Any): Int = {
    person match {
      // RTTI switching and Violating Open / Closed principal
      case PersonCaseClass(name, _) if name.startsWith("Superman") => 100
      case s: String if s.contains("Foo") => 88
      // case someotherclasses ...
      case _ => 0
    }
  }
}