package PatternMatching

// The same as CaseClass, but with an 'actual' class

// Our class ...
class PersonClass(val name: String, val age : Int) {
}

// Companion Object for the class
object PersonClass {
  // Think of apply as a class factory
  def apply(name: String, age : Int) = {
    new PersonClass(name, age)
  }

  def unapply(p: PersonClass): Option[(String, Int)] = {
    Some((p.name, p.age))
  }
}

class MatchClass {
  // Some new random FaceBook BS app "How Cool are you?"
  def rankPerson(person: PersonClass): Int = {
    person match {
      case PersonClass(name, _) if name.startsWith("Superman") => 100
      case PersonClass(name, _) if name.endsWith("Esquire") => 80
      case PersonClass(_, age) if age < 50 => 50
      case PersonClass(name, age) if age > 30 && !name.contains("Idiot") => 20
      case _ => 0
    }
  }
}