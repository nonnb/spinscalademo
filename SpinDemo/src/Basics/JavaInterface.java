package Basics;

// Java dudes don't like the ISomething prefixes on interfaces ...
public interface JavaInterface {
    void doSomething(int x, String y);
}
