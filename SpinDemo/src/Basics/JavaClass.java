package Basics;

public class JavaClass implements JavaInterface {
    @Override
    public void doSomething(int x, String y) {
        System.out.println(String.format("%d - %s", x, y));
    }
}