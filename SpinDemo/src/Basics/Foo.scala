package Basics

object Foo { // i.e. static class
  // public static int subNumbers(x: Int, y: Int) ... we don't need static since the class is static ...
  def addNumbers(x: Int, y: Int) : Int = {
    var z = x;
    z += y;
    return z;
  }

  // Semi colons, return statements are largely redundant ...
  def subtractNumbers(x: Int, y: Int) : Int = {
    // return x - y;
    x - y
  }

  // Method Return type is inferred
  def timesNumbers(x: Int, y: Int) = {
    x * y
  }

  // And a one liner ...
  def divideNumbers(x: Int, y: Int) =  x / y

  // The less ceremony, the better ...
}