package quickcheck

import common._

import org.scalacheck._
import Arbitrary._
import Gen._
import Prop._

import scala.util.Random

abstract class QuickCheckHeap extends Properties("Heap") with IntHeap {

  lazy val genHeap: Gen[H] = {
    val random = new Random()
    val numElems = random.nextInt(500) - 100
    if (numElems <= 0)
      empty
    else {
      val items = (1 to numElems)
        .map(_ => random.nextInt())
      items.foldLeft(empty)((prev, cur) => insert(cur, prev))
    }
  }

  implicit lazy val arbHeap: Arbitrary[H] = Arbitrary(genHeap)

  // Given: i.e. inserting the same minimum as already exists returns the minimum
  property("gen1") = forAll {
    (h: H) =>
      val m = if (isEmpty(h)) 0 else findMin(h)
      findMin(insert(m, h)) == m
  }

  // Given: Inserting any value into an empty heap becomes the minimum
  property("min1") = forAll {
    a: Int =>
      val h = insert(a, empty)
      findMin(h) == a
  }

  // Hint: If you insert any two elements into an empty heap, finding the minimum of the resulting heap should get the smallest of the two elements back.
  property("hint1") = forAll {
    (a: Int, b: Int) =>
      val min = Math.min(a, b)
      val heap = insert(a, insert(b, empty))
      findMin(heap) == min
  }

  // Hint: If you insert an element into an empty heap, then delete the minimum, the resulting heap should be empty
  property("hint2") = forAll {
    a: Int =>
      val heap = insert(a, empty)
      isEmpty(deleteMin(heap)) // i.e. == true - but lose style points if compare to true :(
  }

  // Hint: Given any heap, you should get a sorted sequence of elements when continually finding and deleting minima.
  // Hint: recursion and helper functions are your friends.
  def findMinAndDelete(heap: H) : Stream[Int]= {
    if (isEmpty(heap))
      Stream.empty
    else      {
      findMin(heap) #:: findMinAndDelete(deleteMin(heap))
    }
  }

  property("hint3") = forAll {
    values: List[Int] =>
      val heap = values.foldLeft(empty)((prev, cur) => insert(cur, prev))
      val expectedSequence = values.sorted
      val actualSequence = findMinAndDelete(heap).toList
      actualSequence == expectedSequence
  }

  // Finding a minimum of the melding of any two heaps should return a minimum of one or the other
  property("hint4") = forAll {
    (heap1: H, heap2: H) =>
      val heap1Empty = isEmpty(heap1)
      val heap2Empty = isEmpty(heap2)
      val theMin =
        if(!heap1Empty && !heap2Empty)
          Math.min(findMin(heap1), findMin(heap2))
        else if(heap1Empty && !heap2Empty)
          findMin(heap2)
        else if(!heap1Empty && heap2Empty)
          findMin(heap1)
        else 0

      val melded = meld(heap1, heap2)
      if (heap1Empty && heap2Empty)
        isEmpty(melded)
      else
        findMin(melded) == theMin
  }
}