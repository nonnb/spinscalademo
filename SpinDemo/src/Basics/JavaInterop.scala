package Basics

class JavaInterop extends JavaInterface {
  // Unit is similar to void, but is actually a type
  override def doSomething(x: Int, y: String): Unit = {
    // f interpolation
    println(f"$x%d ... $y%s")

    // s interpolation
    println(s"$x ... $y")
  }

  // Yay ... we can use var for field members, too!
  private var mutableVariable = 5
  private val immutableVariable = "Hello"
  // In .Net we would need to private readonly SomeClass foo = new SomeClass();

  // Both something that Java and Scala can do which .Net can't do ... pop quiz time
  def iCanNewAnInterface(): Unit = {
    val x = new JavaInterface {
      override def doSomething(x: Int, y: String): Unit = {
        println(s"$x ... $y")
      }
    }
  }
}
