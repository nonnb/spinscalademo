package HigherOrderFunctions

class Fun{
  // Scala's functional type inference is better than C#
  val x = (z :Int) => z * z

  // In C# we need to define the Function type explicitly, i.e. either
  // Func<int, int> x = z => z * z
  // OR
  // var x = (Func<int, int>)(z => z * z)
}

class Predicate {
  // Scala - uses [] for generics, and (N) for subscript operator ... unfortunate as it violates the C ancestry
  // Seq[] is like IEnumerable<>
  def applyPredicate[T](myCollection : Seq[T], predicate : T => Boolean) : Seq[T] = {
    // For comprehension))
    for {item <- myCollection
         if (predicate(item))
    }
    yield item

    // C#
    // foreach(var item in myCollection) {
    //     if (predicate(item))
    //        yield return item;
    // }
  }

  def scalaFolding() = {
    val items = Seq(1, 2, 3, 4, 5)

    // Scala implements a fold as a Curry
    val sumItems = items.foldLeft(0)((runningTotal, x) => runningTotal + x)

    // .Net this would be
    // var sum = items.Aggregate(0, (runningTotal, x) => runningTotal + x);

    val sumItemsFromRight = items.foldRight(0)((runningTotal, x) => runningTotal + x)

    // .Net this would be
    // var sum = items.Reverse().Aggregate(0, (runningTotal, x) => runningTotal + x);

    // And yes, scala does have sum
    var sumItems2 = items.sum
  }

//  def applyPredicatePartialFunction[T](myCollection : Seq[T], predicate : T => Boolean) : Seq[T => Boolean] = {
//    for {item <- myCollection
//    }
//    yield predicate
//  }
}

