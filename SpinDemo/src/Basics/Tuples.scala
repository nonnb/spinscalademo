package Basics

class Tuples {
  def dotNetTuple : Unit = {
    val myPair = new Pair[Int, Int](3, 7)
    // ._1 = .item1 etc
    println(s"${myPair._1} ... ${myPair._2}")

    // Tedious, just like .Net
    val anotherTuple = new Tuple5[Int, Int, Int, Int, Int](1,2,3,4,5)
    // Yuk - anotherTuple._1, ... anotherTuple._5
  }

  def scalaTuples = {
    val myPair = (3, 7)
    println(s"${myPair._1} ... ${myPair._2}")

    val myTenTuple = (1, 2, 3, 4, 5, 6, 7, 8, 9, 10)

    // "extraction" of a tuple via unapply
    val (first, second) = incrementPair(3, 4)
    println(s"$first ... $second")
  }

  def incrementPair(pair: (Int, Int)) : (Int, Int) = {
    val (first, second) = pair
    (first + 1, second + 1) // i.e. return new Tuple2[Int, Int](...)
  }
}